package pl.edu.agh.pfpilot.GUI.listeners;

import android.view.View;
import android.view.View.OnLongClickListener;

import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlock;

/**
 * Created by MagdaK on 2015-03-28.
 */
public class NXTBlockInContainerLongClickListener implements OnLongClickListener {

    @Override
    public boolean onLongClick(View view) {
        NXTBlock pressedBlock = (NXTBlock)view;
        startDragging(pressedBlock);

        return true;
    }
    private void startDragging(NXTBlock pressedBlock) {
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(pressedBlock);
        pressedBlock.startDrag(null, shadowBuilder, pressedBlock, 0);
    }
}
