package pl.edu.agh.pfpilot.GUI.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.AttributeSet;

import com.google.gson.Gson;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;

import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlock;
import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlockBreak;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.MotorType;

/**
 * Created by MagdaK on 2015-05-27.
 */
public class NXTBlockSharedPreferences {
    private SharedPreferences sharedPreferences;
    private String preferencesKey = "NXTBlock";

    public NXTBlockSharedPreferences(SharedPreferences sharedPref){
        this.sharedPreferences = sharedPref;
    }

    public List<NXTBlock> getSavedNXTBlocks(Context applicationContext){
        List<NXTBlock> listOfNXTBlocks = new ArrayList<>();
        Gson gson = new Gson();
        String json = sharedPreferences.getString(preferencesKey, "");
        NXTBlockSharedPreference[] nxtBlockPreferenceList = gson.fromJson(json, NXTBlockSharedPreference[].class);

        if (nxtBlockPreferenceList != null) {
            for (NXTBlockSharedPreference nxtBlockPreference : nxtBlockPreferenceList) {
                NXTBlock nxtBlock = convertToNXTBlock(nxtBlockPreference, applicationContext);
                if (nxtBlock != null) {
                    listOfNXTBlocks.add(nxtBlock);
                }
            }
        }

        return listOfNXTBlocks;
    }

    public void saveSequencePanelInPreferences(List<NXTBlock> nxtBlocks){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();

        List<NXTBlockSharedPreference> listOfPreferences = new ArrayList<>();
        for (NXTBlock nxtBlock : nxtBlocks){
            listOfPreferences.add(convertToPreference(nxtBlock));
        }

        String json = gson.toJson(listOfPreferences);
        editor.putString(preferencesKey, json);
        editor.commit();
    }

    private NXTBlockSharedPreference convertToPreference(NXTBlock nxtBlock){
        NXTBlockSharedPreference nxtBlockPreference = new NXTBlockSharedPreference();
        nxtBlockPreference.className = nxtBlock.getClass().getName();
        nxtBlockPreference.motorType = nxtBlock.getMotorType().toString();

        return nxtBlockPreference;
    }

    private NXTBlock convertToNXTBlock(NXTBlockSharedPreference nxtBlockPreference, Context applicationContext){
        NXTBlock nxtBlock = null;

        try {
            Class<?> clazz = Class.forName(nxtBlockPreference.className);
            Constructor<?> constructor = clazz.getConstructor(Context.class, AttributeSet.class);
            nxtBlock = (NXTBlock)constructor.newInstance(applicationContext, null);

            MotorType motorType = Enum.valueOf(MotorType.class, nxtBlockPreference.motorType);
            nxtBlock.setMotorType(motorType);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        return nxtBlock;
    }
}
