package pl.edu.agh.pfpilot.GUI.listeners;

import android.view.View;

import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlock;

/**
 * Created by MagdaK on 2015-03-28.
 */
public class NXTBlockClickListener implements View.OnClickListener{

    @Override
    public void onClick(View v) {
        NXTBlock nxtBlock = (NXTBlock)v;
        nxtBlock.setNextImage();
    }
}
