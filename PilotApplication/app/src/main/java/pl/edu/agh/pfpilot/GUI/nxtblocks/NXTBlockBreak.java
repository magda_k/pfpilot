package pl.edu.agh.pfpilot.GUI.nxtblocks;

import android.content.Context;
import android.util.AttributeSet;

import pl.edu.agh.gui.R;

/**
 * Created by MagdaK on 2015-05-01.
 */
public class NXTBlockBreak extends NXTBlock {

    public NXTBlockBreak(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
    }

    @Override
    protected void setImages() {
        addImage("motor_break_a", R.drawable.motor_break_a);
        addImage("motor_break_b", R.drawable.motor_break_b);
        addImage("motor_break_ab", R.drawable.motor_break_ab);
    }

    @Override
    public int[] getCommand() {
        return rcProtocol.motorBreak(this.getMotorType());
    }
}