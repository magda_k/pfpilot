package pl.edu.agh.pfpilot.RCProtocolApi;

import android.util.Log;

import java.util.Arrays;

import pl.edu.agh.pfpilot.RCProtocolApi.commands.*;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Channel;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.MotorType;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Toggle;

/**
 * Created by MagdaK on 2015-03-29.
 */
public class RCProtocol {

    //region Fields

    private static RCProtocol instance = null;
    private Toggle toggle = new Toggle();
    private Channel channel = Channel.CHANNEL_1;

    private Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    //endregion

    //region Constructor

    private RCProtocol() {}

    public static RCProtocol getInstance() {
        if (instance == null) {
            instance = new RCProtocol();
        }
        return instance;
    }

    //endregion

    //region Commands

    public int[] motorForward(MotorType motorType){
        return getCommendImplementation(new MotorForward(motorType));
    }

    public int[] motorBackward(MotorType motorType){
        return getCommendImplementation(new MotorBackward(motorType));
    }

    public int[] speedPlus(MotorType motorType){
        return getCommendImplementation(new SpeedPlus(motorType));
    }

    public int[] speedMinus(MotorType motorType){
        return getCommendImplementation(new SpeedMinus(motorType));
    }

    public int[] motorBreak(MotorType motorType){
        return getCommendImplementation(new MotorBreak(motorType));
    }

    //endregion

    //region Utils

    private int[] getCommendImplementation(ICommand command){
        return command.getImplementation(toggle, getChannel());
    }

    //endregion
}
