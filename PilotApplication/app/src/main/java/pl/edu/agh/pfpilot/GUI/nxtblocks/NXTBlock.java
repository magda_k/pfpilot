package pl.edu.agh.pfpilot.GUI.nxtblocks;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import pl.edu.agh.pfpilot.GUI.listeners.NXTBlockClickListener;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.MotorType;
import pl.edu.agh.pfpilot.RCProtocolApi.RCProtocol;

/**
 * Created by MagdaK on 2015-03-21.
 */
public abstract class NXTBlock extends ImageView {

    protected List<NXTBlockType> _liResources = new LinkedList<>();
    protected RCProtocol rcProtocol = RCProtocol.getInstance();
    private MotorType _motorType;
    private ListIterator<NXTBlockType> it;

    public NXTBlock(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);

        setLayoutParameters();
        setClickListener();
        setImages();
        setIterator();
        setNextImage();
    }

    private void setLayoutParameters(){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        params.weight = 1;
        params.setMargins(5, 0, 5, 0);
        this.setLayoutParams(params);
    }

    private void setClickListener(){
        this.setClickable(true);
        this.setOnClickListener(new NXTBlockClickListener());
    }

    private void setIterator(){
        it = _liResources.listIterator();
    }

    protected abstract void setImages();

    public abstract int[] getCommand();

    public MotorType getMotorType(){
        return _motorType;
    }

    public void setMotorType(MotorType motorType){
        _motorType = motorType;

        for (NXTBlockType nxtType : _liResources)
        {
            if (nxtType.MotorType == motorType)
            {
                this.setImageResource(nxtType.ResourceId);
            }
        }
    }

    public void setNextImage(){
        if (!it.hasNext()){
            setIterator();
        }

        NXTBlockType type = it.next();
        setMotorType(type.MotorType);
    }

    protected void addImage(String imgName, Integer imgId){
        MotorType motorType =  MotorType.MotorA;
        if (imgName.endsWith("ab")){
            motorType = MotorType.MotorAB;
        }
        else if (imgName.endsWith("a")) {
            motorType = MotorType.MotorA;
        }
        else if (imgName.endsWith("b")){
            motorType = MotorType.MotorB;
        }

        _liResources.add(new NXTBlockType(imgId, motorType));
    }

    public void setHighlight()
    {
        this.setImageAlpha(100);
        this.invalidate();
    }

    public void removeHighlight()
    {
        this.setImageAlpha(255);
        this.invalidate();
    }
}
