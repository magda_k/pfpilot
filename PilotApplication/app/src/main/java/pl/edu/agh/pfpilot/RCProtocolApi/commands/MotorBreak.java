package pl.edu.agh.pfpilot.RCProtocolApi.commands;

import pl.edu.agh.pfpilot.RCProtocolApi.helpers.*;
import pl.edu.agh.pfpilot.RCProtocolApi.rcdata.SingleOutputModeData;

/**
 * Created by MagdaK on 2015-05-01.
 */
public class MotorBreak extends CommandWithMotorType {

    public MotorBreak(MotorType motorType) {
        super(motorType);
    }

    @Override
    public int[] getImplementation(Toggle toggle, Channel channel) {
        return getSingleOutputModeRCOrderByMotor(toggle, channel, SingleOutputModeData.BREAK_THEN_FLOAT);
    }
}
