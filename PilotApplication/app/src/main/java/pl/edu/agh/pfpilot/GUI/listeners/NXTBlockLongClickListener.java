package pl.edu.agh.pfpilot.GUI.listeners;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlock;
import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlockSpeedPlus;

import android.view.View.OnLongClickListener;

import java.lang.reflect.Constructor;

/**
 * Created by MagdaK on 2015-03-28.
 */
public class NXTBlockLongClickListener implements OnLongClickListener {

    @Override
    public boolean onLongClick(View view) {
        NXTBlock pressedBlock = (NXTBlock)view;
        NXTBlock blockCopy = createCopyOfNXTBlock(pressedBlock);

        addCopyToPressedBlockContainer(pressedBlock, blockCopy);
        startDragging(pressedBlock, blockCopy);

        return true;
    }

    private void startDragging(NXTBlock pressedBlock, NXTBlock blockCopy) {
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(pressedBlock);
        blockCopy.startDrag(null, shadowBuilder, blockCopy, 0);
    }

    private void addCopyToPressedBlockContainer(NXTBlock pressedBlock, NXTBlock blockCopy) {
        ViewGroup from = (ViewGroup) pressedBlock.getParent();
        from.addView(blockCopy);
    }

    private NXTBlock createCopyOfNXTBlock(NXTBlock pressedBlock)
    {
        Context applicationContext = pressedBlock.getContext();
        String className = pressedBlock.getClass().getName();

        NXTBlock blockCopy = new NXTBlockSpeedPlus(applicationContext, null);
        try {
            Class<?> clazz = Class.forName(className);
            Constructor<?> constructor = clazz.getConstructor(Context.class, AttributeSet.class);
            Object object = constructor.newInstance(applicationContext, null);

            blockCopy = (NXTBlock)object;
            blockCopy.setMotorType(pressedBlock.getMotorType());
        } catch (Exception e) {
            e.printStackTrace();
        }

        blockCopy.setOnLongClickListener(new NXTBlockInContainerLongClickListener());
        blockCopy.setVisibility(View.GONE);

        return blockCopy;
    }
}
