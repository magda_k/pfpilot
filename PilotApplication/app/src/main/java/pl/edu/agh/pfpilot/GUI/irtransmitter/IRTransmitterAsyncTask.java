package pl.edu.agh.pfpilot.GUI.irtransmitter;

import android.os.AsyncTask;

import java.util.List;

import pl.edu.agh.pfpilot.GUI.helpers.MessageHelper;
import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlock;

/**
 * Created by MagdaK on 2015-03-29.
 */
public class IRTransmitterAsyncTask extends AsyncTask<Void, IRTransmitterPublishParams, Boolean> {

    private IRTransmitter transmitter;
    private List<NXTBlock> blockList;

    public IRTransmitterAsyncTask(IRTransmitter transmitter, List<NXTBlock> blockList) {
        this.transmitter = transmitter;
        this.blockList = blockList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        for (NXTBlock nxtBlock: blockList)
        {
            if (!isCancelled()) {
                publishProgress(new IRTransmitterPublishParams(nxtBlock, true));
                transmitter.transmit(nxtBlock.getCommand());
                publishProgress(new IRTransmitterPublishParams(nxtBlock, false));
            }
        }
        return true;
    }

    protected void onProgressUpdate(IRTransmitterPublishParams... params) {
        IRTransmitterPublishParams param = params[0];
        NXTBlock nxtBlock = param.nxtBlock;
        boolean setHighlight = param.setHighlight;

        if (setHighlight) {
            nxtBlock.setHighlight();
        }
        else {
            nxtBlock.removeHighlight();
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        MessageHelper.getInstance().showMessage("Sending finished.");
    }
}
