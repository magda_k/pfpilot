package pl.edu.agh.pfpilot.GUI.irtransmitter;

import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlock;

/**
 * Created by MagdaK on 2015-03-29.
 */
public class IRTransmitterParameters {

    public int frequency;
    public int[] pattern;

    public IRTransmitterParameters(int[] pattern, int frequency) {
        this.pattern = pattern;
        this.frequency = frequency;
    }

    public IRTransmitterParameters(int[] pattern) {
        this(pattern, 0);
    }
}
