package pl.edu.agh.pfpilot.RCProtocolApi.rcorders;

import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Channel;
import pl.edu.agh.pfpilot.RCProtocolApi.rcdata.ComboDirectModeData;

/**
 * Created by Przemek on 2015-04-10.
 */
public class ComboDirectModeRCOrder extends RCOrder {

    /**
     * Implementation of the Combo Direct Mode.
     *
     * @param toggle - toggle
     * @param channel - channel
     * @param data - data
     */
    public ComboDirectModeRCOrder(int toggle, Channel channel, ComboDirectModeData data) {
        this.setToggle(toggle);
        this.setEscape(0);
        this.setChannel(channel);

        nibble3 = data.getData();

        this.setLrcCheck();
        this.fillInstance();
    }
}