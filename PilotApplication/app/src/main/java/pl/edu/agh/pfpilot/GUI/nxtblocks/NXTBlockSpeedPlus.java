package pl.edu.agh.pfpilot.GUI.nxtblocks;

import android.content.Context;
import android.util.AttributeSet;

import pl.edu.agh.gui.R;

/**
 * Created by MagdaK on 2015-03-28.
 */
public class NXTBlockSpeedPlus extends NXTBlock {

    public NXTBlockSpeedPlus(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
    }

    @Override
    protected void setImages() {
        addImage("speed_plus_a", R.drawable.speed_plus_a);
        addImage("speed_plus_b", R.drawable.speed_plus_b);
        addImage("speed_plus_ab", R.drawable.speed_plus_ab);
    }

    @Override
    public int[] getCommand() {
        return rcProtocol.speedPlus(this.getMotorType());
    }
}
