package pl.edu.agh.pfpilot.GUI;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.hardware.ConsumerIrManager;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.LinkedList;
import java.util.List;

import pl.edu.agh.gui.R;
import pl.edu.agh.pfpilot.GUI.helpers.NXTBlockSharedPreference;
import pl.edu.agh.pfpilot.GUI.helpers.NXTBlockSharedPreferences;
import pl.edu.agh.pfpilot.GUI.irtransmitter.IRTransmitter;
import pl.edu.agh.pfpilot.GUI.irtransmitter.IRTransmitterAsyncTask;
import pl.edu.agh.pfpilot.GUI.helpers.MessageHelper;
import pl.edu.agh.pfpilot.GUI.listeners.NXTBlockInContainerLongClickListener;
import pl.edu.agh.pfpilot.GUI.listeners.NXTBlockLongClickListener;
import pl.edu.agh.pfpilot.GUI.listeners.PFPilotDragListener;
import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlock;
import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlockBreak;
import pl.edu.agh.pfpilot.RCProtocolApi.RCProtocol;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Channel;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.MotorType;

public class MainActivity extends ActionBarActivity {

    private IRTransmitter transmitter;
    private RCProtocol rcProtocol = RCProtocol.getInstance();
    private IRTransmitterAsyncTask sendAsyncTask;
    private NXTBlockSharedPreferences sharedPreferences;
    //region Life cycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTopPanelListeners();
        setSequencePanelListener();
        setBinListener();
        MessageHelper.getInstance().setApplicationContext(this.getApplicationContext());
        initIRTransmitter();
        readFromSharedPreferences();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sharedPreferences.saveSequencePanelInPreferences(getNXTBlocksFromSequencePanel());
    }
    //endregion

    //region Listeners

    private void setBinListener() {
        findViewById(R.id.bin).setOnDragListener(new PFPilotDragListener());
    }

    private void setSequencePanelListener() {
        findViewById(R.id.sequencePanel).setOnDragListener(new PFPilotDragListener());
    }

    private void setTopPanelListeners(){
        LinearLayout topPanel = (LinearLayout)findViewById(R.id.topPanel);
        for (int i=0; i < topPanel.getChildCount(); ++i) {
            View nxtBlock = topPanel.getChildAt(i);
            nxtBlock.setOnLongClickListener(new NXTBlockLongClickListener());
        }
    }

    public void onStartButtonClick(View view) {
        actionSend();
    }

    public void onEndButtonClick(View view) {
        actionEnd();
    }

    //endregion Listeners

    //region IrTransmitter

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void initIRTransmitter() {
        ConsumerIrManager irManager = (ConsumerIrManager)getSystemService(CONSUMER_IR_SERVICE);
        transmitter = new IRTransmitter(irManager);

        if (!transmitter.hasIrEmitter()) {
            MessageHelper.getInstance().showMessage("IR transmitter not found. Application may not work correctly.");
        }
    }

    //endregion

    //region Menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        setChannel(menu.findItem(R.id.channel_1), Channel.CHANNEL_1);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send:
                actionSend();
                return true;
            case R.id.action_clear:
                actionClear();
                return true;
            case R.id.channel_1:
                return setChannel(item, Channel.CHANNEL_1);
            case R.id.channel_2:
                return setChannel(item, Channel.CHANNEL_2);
            case R.id.channel_3:
                return setChannel(item, Channel.CHANNEL_3);
            case R.id.channel_4:
                return setChannel(item, Channel.CHANNEL_4);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean setChannel(MenuItem item, Channel channel){
        item.setChecked(true);
        rcProtocol.setChannel(channel);
        return true;
    }

    private void actionSend(){
        List<NXTBlock> blockList = getNXTBlocksFromSequencePanel();
        this.sendAsyncTask = new IRTransmitterAsyncTask(transmitter, blockList);
        this.sendAsyncTask.execute();
    }

    private void actionEnd() {
        if (this.sendAsyncTask != null && !this.sendAsyncTask.isCancelled()){
            this.sendAsyncTask.cancel(true);
            MessageHelper.getInstance().showMessage("Action cancelled.");

            ViewGroup sequencePanel = (ViewGroup)findViewById(R.id.sequencePanel);
            for (int i=0; i < sequencePanel.getChildCount(); ++i) {
                NXTBlock nxtBlock = (NXTBlock)sequencePanel.getChildAt(i);
                nxtBlock.removeHighlight();
            }
        }

        NXTBlock breakCommand = new NXTBlockBreak(this.getApplicationContext(), null);
        breakCommand.setMotorType(MotorType.MotorAB);
        transmitter.transmit(breakCommand.getCommand());
    }

    private void actionClear(){
        ViewGroup sequencePanel = (ViewGroup)findViewById(R.id.sequencePanel);
        sequencePanel.removeAllViews();
    }


    private List<NXTBlock> getNXTBlocksFromSequencePanel(){
        ViewGroup sequencePanel = (ViewGroup)findViewById(R.id.sequencePanel);
        List<NXTBlock> blockList = new LinkedList<NXTBlock>();
        for (int i=0; i < sequencePanel.getChildCount(); ++i) {
            NXTBlock nxtBlock = (NXTBlock)sequencePanel.getChildAt(i);
            blockList.add(nxtBlock);
        }

        return blockList;
    }

    private void readFromSharedPreferences(){
        sharedPreferences = new NXTBlockSharedPreferences(getPreferences(MODE_PRIVATE));
        ViewGroup sequencePanel = (ViewGroup)findViewById(R.id.sequencePanel);
        List<NXTBlock> blockList = sharedPreferences.getSavedNXTBlocks(this.getApplicationContext());

        for (NXTBlock nxtBlock : blockList) {
            nxtBlock.setOnLongClickListener(new NXTBlockInContainerLongClickListener());
            sequencePanel.addView(nxtBlock);
        }

    }

    //endregion
}
