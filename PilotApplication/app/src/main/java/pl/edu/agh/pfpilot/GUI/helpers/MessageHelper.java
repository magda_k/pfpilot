package pl.edu.agh.pfpilot.GUI.helpers;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by MagdaK on 2015-03-29.
 */
public class MessageHelper {

    //region Singleton

    private static MessageHelper instance = null;
    private Context applicationContext;

    public static MessageHelper getInstance() {
        if (instance == null) {
            instance = new MessageHelper();
        }
        return instance;
    }

    //endregion

    public void setApplicationContext(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void showMessage(String message) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show();
    }
}
