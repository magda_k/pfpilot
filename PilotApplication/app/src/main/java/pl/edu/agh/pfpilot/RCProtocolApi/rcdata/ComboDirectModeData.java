package pl.edu.agh.pfpilot.RCProtocolApi.rcdata;

/**
 * Created by MagdaK on 2015-05-01.
 *
 * Implementation of the Combo Direct Mode data nibble.
 *
 * 00 Float output
 * 01 Forward on output
 * 10 Backward on output
 * 11 Brake then float output
 */
public enum ComboDirectModeData {
    FLOAT_OUTPUT (0,0),
    FORWARD_OUTPUT (0,1),
    BACKWARD_OUTPUT (1,0),
    BRAKE_THEN_FLOAT_OUTPUT (1,1);

    private final int[] data;

    ComboDirectModeData(int bit1, int bit2) {
        this.data = new int[] {bit1, bit2 };
    }

    public int[] getData() {
        return data;
    }
}
