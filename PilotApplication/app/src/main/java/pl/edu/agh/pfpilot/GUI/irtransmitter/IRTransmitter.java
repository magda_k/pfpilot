package pl.edu.agh.pfpilot.GUI.irtransmitter;

import android.hardware.ConsumerIrManager;
import android.os.AsyncTask;
import android.util.Log;

import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlock;
import java.util.Arrays;


public class IRTransmitter{

    //region Fields

    private ConsumerIrManager transmitter;
    private boolean hasEmitter;
    private static final int FREQ = 38028;

    //endregion

    //region Constructor

    public IRTransmitter(ConsumerIrManager emitter) {
        transmitter = emitter;
        hasEmitter = emitter.hasIrEmitter();
    }

    //endregion

    public boolean hasIrEmitter() {
        return hasEmitter;
    }

    public void transmit(int[] pattern) {
        transmit(FREQ, pattern);
    }

    public void transmit(int carriedFrequency, int[] pattern) {
        if (hasEmitter) {
            transmitter.transmit(carriedFrequency, pattern);
        }

        //@magda 29-03-2015 tmp. symuluje delay przy przesyłaniu
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
