package pl.edu.agh.pfpilot.GUI.nxtblocks;

import pl.edu.agh.pfpilot.RCProtocolApi.helpers.MotorType;

/**
 * Created by MagdaK on 2015-03-28.
 */
public class NXTBlockType {
    //region Fields

    public MotorType MotorType;
    public Integer ResourceId;

    //endregion

    //region Constructor

    public NXTBlockType(Integer resourceId, MotorType motorType) {
        ResourceId = resourceId;
        MotorType = motorType;
    }

    //endregion
}

