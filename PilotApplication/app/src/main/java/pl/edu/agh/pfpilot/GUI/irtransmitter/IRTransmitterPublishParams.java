package pl.edu.agh.pfpilot.GUI.irtransmitter;

import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlock;

/**
 * Created by MagdaK on 2015-03-29.
 */
public class IRTransmitterPublishParams {

    public NXTBlock nxtBlock;
    public boolean setHighlight;

    public IRTransmitterPublishParams(NXTBlock nxtBlock, boolean setHighlight) {
        this.nxtBlock = nxtBlock;
        this.setHighlight = setHighlight;
    }
}
