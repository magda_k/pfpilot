package pl.edu.agh.pfpilot.RCProtocolApi.helpers;

/**
 * Created by MagdaK on 2015-03-28.
 */
public enum MotorType
{
    MotorA,
    MotorB,
    MotorAB
}
