package pl.edu.agh.pfpilot.RCProtocolApi.rcdata;

/**
 * Created by MagdaK on 2015-05-01.
 *
 * Implementation of the Single Output Mode data nibble.
 *
 * Mode = PWM
 *
 * Data DDDD 0000 Float
 * 0001 PWM forward step 1
 * 0010 PWM forward step 2
 * 0011 PWM forward step 3
 * 0100 PWM forward step 4
 * 0101 PWM forward step 5
 * 0110 PWM forward step 6
 * 0111 PWM forward step 7
 * 1000 Brake then float
 * 1001 PWM backward step 7
 * 1010 PWM backward step 6
 * 1011 PWM backward step 5
 * 1100 PWM backward step 4
 * 1101 PWM backward step 3
 * 1110 PWM backward step 2
 * 1111 PWM backward step 1
 *
 * Mode = Clear/Set/Toggle/Inc/Dec
 *
 * 0000 Toggle full forward (Stop → Fw, Fw → Stop, Bw → Fw)
 * 0001 Toggle direction
 * 0010 Increment numerical PWM
 * 0011 Decrement numerical PWM
 * 0100 Increment PWM
 * 0101 Decrement PWM
 * 0110 Full forward (timeout)
 * 0111 Full backward (timeout)
 * 1000 Toggle full forward/backward (default forward)
 * 1001 Clear C1 (negative logic – C1 high)
 * 1010 Set C1 (negative logic – C1 low)
 * 1011 Toggle C1
 * 1100 Clear C2 (negative logic – C2 high)
 * 1101 Set C2 (negative logic – C2 low)
 * 1110 Toggle C2
 * 1111 Toggle full backward (Stop → Bw, Bw → Stop, Fwd → Bw)
 */
public enum SingleOutputModeData {
    //Mode = PWM
    FLOAT (0,0,0,0),
    PWM_FORWARD_1 (0,0,0,1),
    PWM_FORWARD_2 (0,0,1,0),
    PWM_FORWARD_3 (0,0,1,1),
    PWM_FORWARD_4 (0,1,0,0),
    PWM_FORWARD_5 (0,1,0,1),
    PWM_FORWARD_6 (0,1,1,0),
    PWM_FORWARD_7 (0,1,1,1),
    BREAK_THEN_FLOAT (1,0,0,0),
    PWM_BACKWARD_7 (1,0,0,1),
    PWM_BACKWARD_6 (1,0,1,0),
    PWM_BACKWARD_5 (1,0,1,1),
    PWM_BACKWARD_4 (1,1,0,0),
    PWM_BACKWARD_3 (1,1,0,1),
    PWM_BACKWARD_2 (1,1,1,0),
    PWM_BACKWARD_1 (1,1,1,1),

    //Mode = Clear/Set/Toggle/Inc/Dec
    TOGGLE_FULL_FORWARD (0,0,0,0),
    TOGGLE_DIRECTION (0,0,0,1),
    INCREMENT_NUMERICAL_PWM (0,0,1,0),
    DECREMENT_NUMERICAL_PWM (0,0,1,1),
    INCREMENT_PWM (0,1,0,0),
    DECREMENT_PWM (0,1,0,1),
    FULL_FORWARD (0,1,1,0),        //(timeout)
    FULL_BACKWARD (0,1,1,1),        //(timeout)
    TOGGLE_FULL_FORWARD_OR_BACKWARD (1,0,0,0),
    CLEAR_C1 (1,0,0,1),
    SET_C1 (1,0,1,0),
    TOGGLE_C1 (1,0,1,1),
    CLEAR_C2 (1,1,0,0),
    SET_C2 (1,1,0,1),
    TOGGLE_C2 (1,1,1,0),
    TOGGLE_FULL_BACKWARD (1,1,1,1);

    private final int[] data;

    SingleOutputModeData(int bit1, int bit2, int bit3, int bit4) {
        this.data = new int[] {bit1, bit2, bit3, bit4 };
    }

    public int[] getData() {
        return data;
    }
}
