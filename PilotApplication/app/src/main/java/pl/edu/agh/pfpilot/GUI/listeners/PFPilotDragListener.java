package pl.edu.agh.pfpilot.GUI.listeners;

import android.graphics.Rect;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnDragListener;

import pl.edu.agh.gui.R;
import pl.edu.agh.pfpilot.GUI.nxtblocks.NXTBlock;

/**
 * Created by MagdaK on 2015-03-28.
 */
public class PFPilotDragListener implements OnDragListener {

    @Override
    public boolean onDrag(View view, DragEvent e) {
        switch (e.getAction()) {

            case DragEvent.ACTION_DROP:
                View element = (View) e.getLocalState();
                int x = Math.round(e.getX());
                int y = Math.round(e.getY());

                checkIfElementAddedToSequencePanel(element, view, x, y);
                checkIfElementAddedToBin(element, view);

                break;

            default:
                break;
        }

        return true;
    }

    private void checkIfElementAddedToBin(View element, View droppedOn) {
        if (droppedOn.getId() == R.id.bin){
            removeElementFromParent(element);
        }
    }

    private void checkIfElementAddedToSequencePanel(View element,  View droppedOn, int x, int y) {
        if (droppedOn.getId() == R.id.sequencePanel){
            ViewGroup container = (ViewGroup) droppedOn;

            int index = container.getChildCount() - 1;

            for (int i=0; i < container.getChildCount(); ++i) {
                View _child = container.getChildAt(i);
                Rect _bounds = new Rect();
                _child.getHitRect(_bounds);

                if (_bounds.contains(x, y)) {
                    index = i;
                    if (index == container.getChildCount() - 1 && !parentHasElement(container, element)){
                        index++;
                    }
                }
            }

            removeElementFromParent(element);
            container.addView(element, index);
            element.setVisibility(View.VISIBLE);
        }
    }

    private boolean parentHasElement(ViewGroup parent, View element){
        boolean hasElement = false;
        for (int i=0; i < parent.getChildCount(); ++i) {
            NXTBlock nxtBlock = (NXTBlock)parent.getChildAt(i);

            if (nxtBlock == element){
                return true;
            }
        }

        return hasElement;
    }

    private void removeElementFromParent(View element) {
        ViewGroup from = (ViewGroup) element.getParent();
        from.removeView(element);
    }
}
