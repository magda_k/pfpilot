package pl.edu.agh.pfpilot.RCProtocolApi.rcdata;

/**
 * Created by MagdaK on 2015-05-01.
 *
 * Implementation of the Extended Mode data nibble.
 *
 * 0000 Brake then float output A
 * 0001 Increment speed on output A
 * 0010 Decrement speed on output A
 * 0011 Not used
 * 0100 Toggle forward/float on output B
 * 0101 Not used
 * 0110 Toggle Address bit
 * 0111 Align toggle bit (get in sync)
 * 1000 Reserved
 */
public enum ExtendedModeData {
    BRAKE_THEN_FLOAT_OUTPUT_A (0,0,0,0),
    INCREMENT_SPEED_OUTPUT_A (0,0,0,1),
    DECREMENT_SPEED_OUTPUT_A (0,0,1,0),
    TOGGLE_FORWARD_OR_FLOAT_OUTPUT_ (0,1,0,0),
    TOGGLE_ADDRESS_BIT (0,1,1,0),
    ALIGN_TOGGLE_BIT (0,1,1,1);

    private final int[] data;

    ExtendedModeData(int bit1, int bit2, int bit3, int bit4) {
        this.data = new int[] {bit1, bit2, bit3, bit4};
    }

    public int[] getData() {
        return data;
    }
}
