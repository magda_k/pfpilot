package pl.edu.agh.pfpilot.RCProtocolApi.rcorders;

import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Channel;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.OutputType;
import pl.edu.agh.pfpilot.RCProtocolApi.rcdata.SingleOutputModeData;

/**
 * Created by Przemek on 2015-04-10.
 */
public class SingleOutputModeRCOrder extends RCOrder {

    /**
     * Implementation of the Extended Mode.
     *
     * @param toggle - toggle
     * @param channel - channel
     * @param output - 0 - motor A, 1 - motor B
     * @param data - data
     */
    public SingleOutputModeRCOrder(int toggle, Channel channel, OutputType output, SingleOutputModeData data) {
        this.setToggle(toggle);
        this.setEscape(0);
        this.setChannel(channel);

        nibble2[0] = 0;
        nibble2[1] = 1;
        nibble2[2] = this.getMode(data);
        nibble2[3] = output.getOutput();

        nibble3 = data.getData();

        this.setLrcCheck();
        this.fillInstance();
    }

    /**
     * Gets mode bit by the type of the command.
     *
     * @param data - data
     * @return mode bit.
     */
    private int getMode(SingleOutputModeData data) {

        if (data == SingleOutputModeData.FLOAT ||data == SingleOutputModeData.PWM_FORWARD_1 ||
                data == SingleOutputModeData.PWM_FORWARD_2 || data == SingleOutputModeData.PWM_FORWARD_3 ||
                data == SingleOutputModeData.PWM_FORWARD_4 || data == SingleOutputModeData.PWM_FORWARD_5 ||
                data == SingleOutputModeData.PWM_FORWARD_6 || data == SingleOutputModeData.PWM_FORWARD_7 ||
                data == SingleOutputModeData.BREAK_THEN_FLOAT || data == SingleOutputModeData.PWM_BACKWARD_1 ||
                data == SingleOutputModeData.PWM_BACKWARD_2 || data == SingleOutputModeData.PWM_BACKWARD_3 ||
                data == SingleOutputModeData.PWM_BACKWARD_4 || data == SingleOutputModeData.PWM_BACKWARD_5 ||
                data == SingleOutputModeData.PWM_BACKWARD_6 || data == SingleOutputModeData.PWM_BACKWARD_7){
            return 0;
        }
        else {
            return 1;
        }
    }
}

