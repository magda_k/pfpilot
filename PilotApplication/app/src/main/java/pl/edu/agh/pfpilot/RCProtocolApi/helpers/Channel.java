package pl.edu.agh.pfpilot.RCProtocolApi.helpers;

/**
 * Created by MagdaK on 2015-05-20.
 */

public enum Channel {
    CHANNEL_1 (0,0),
    CHANNEL_2 (0,1),
    CHANNEL_3 (1,0),
    CHANNEL_4 (1,1);

    private final int[] channel;

    Channel(int bit1, int bit2) {
        this.channel = new int[] { bit1, bit2 };
    }

    public int[] getChannel() {
        return channel;
    }
}