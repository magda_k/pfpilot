package pl.edu.agh.pfpilot.RCProtocolApi.helpers;

/**
 * Created by MagdaK on 2015-05-21.
 */
public class Toggle {
    private boolean toggle = true;

    public synchronized int getToggle() {
        toggle = !toggle;
        return toggle ? 1 : 0;
    }
}
