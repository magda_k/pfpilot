package pl.edu.agh.pfpilot.RCProtocolApi.rcdata;

/**
 * Created by MagdaK on 2015-05-01.
 *
 *    Implementation of the Combo PWM Mode data nibble.
        *
        *    0000 Float
        *    0001 PWM forward step 1
        *    0010 PWM forward step 2
        *    0011 PWM forward step 3
        *    0100 PWM forward step 4
        *    0101 PWM forward step 5
        *    0110 PWM forward step 6
        *    0111 PWM forward step 7
        *    1000 Brake then float
        *    1001 PWM backward step 7
        *    1010 PWM backward step 6
        *    1011 PWM backward step 5
        *    1100 PWM backward step 4
        *    1101 PWM backward step 3
        *    1110 PWM backward step 2
        *    1111 PWM backward step 1
        */
public enum ComboPWMModeData {

    FLOAT (0,0,0,0),
    PWM_FORWARD_1 (0,0,0,1),
    PWM_FORWARD_2 (0,0,1,0),
    PWM_FORWARD_3 (0,0,1,1),
    PWM_FORWARD_4 (0,1,0,0),
    PWM_FORWARD_5 (0,1,0,1),
    PWM_FORWARD_6 (0,1,1,0),
    PWM_FORWARD_7 (0,1,1,1),
    BREAK_THEN_FLOAT (1,0,0,0),
    PWM_BACKWARD_7 (1,0,0,1),
    PWM_BACKWARD_6 (1,0,1,0),
    PWM_BACKWARD_5 (1,0,1,1),
    PWM_BACKWARD_4 (1,1,0,0),
    PWM_BACKWARD_3 (1,1,0,1),
    PWM_BACKWARD_2 (1,1,1,0),
    PWM_BACKWARD_1 (1,1,1,1);

    private final int[] data;

    ComboPWMModeData(int bit1, int bit2, int bit3, int bit4) {
        this.data = new int[] {bit1, bit2, bit3, bit4 };
    }

    public int[] getData() {
        return data;
    }
}
