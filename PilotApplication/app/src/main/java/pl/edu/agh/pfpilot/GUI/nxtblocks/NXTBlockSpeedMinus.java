package pl.edu.agh.pfpilot.GUI.nxtblocks;

import android.content.Context;
import android.util.AttributeSet;

import pl.edu.agh.gui.R;

/**
 * Created by MagdaK on 2015-03-28.
 */
public class NXTBlockSpeedMinus extends NXTBlock{

    public NXTBlockSpeedMinus(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
    }

    @Override
    protected void setImages() {
        addImage("speed_minus_a", R.drawable.speed_minus_a);
        addImage("speed_minus_b", R.drawable.speed_minus_b);
        addImage("speed_minus_ab", R.drawable.speed_minus_ab);
    }

    @Override
    public int[] getCommand() {
        return rcProtocol.speedMinus(this.getMotorType());
    }
}
