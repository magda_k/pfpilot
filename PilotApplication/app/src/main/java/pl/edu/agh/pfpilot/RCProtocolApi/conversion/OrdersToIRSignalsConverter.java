package pl.edu.agh.pfpilot.RCProtocolApi.conversion;

import android.util.Log;

import java.util.Arrays;

import pl.edu.agh.pfpilot.RCProtocolApi.rcorders.RCOrder;

/**
 * Created by Przemek on 2015-03-31.
 */
public class OrdersToIRSignalsConverter {

    /**
     * Length of IR signal = 157 us.
     */
    private static int IRSignal = 157;

    /**
     * LowBitLength = 421 us, 6 cycles of IR and 10 “cycles” of pause,
     */
    private static int LowBitPauseLength = 263;

    /**
     * HighBitLength = 711 us, 6 cycles IR and 21 “cycles” of pause
     */
    private static int HighBitPauseLength = 552;

    /**
     * StartStopBitLength = 1184 us, 6 cycles IR and 39 “cycles” of pause.
     */
    private static int StartStopBitPauseLength = 1026;

    /**
     * Converts RC protocol binary command into IR pattern.
     *
     * @param order - binary RC command.
     * @return IR pattern to transmit.
     */
    public static int[] convert (RCOrder order) {

        int[] irPattern = new int[36];
        irPattern[0] = IRSignal;
        irPattern[1] = StartStopBitPauseLength;
        irPattern[34] = IRSignal;
        irPattern[35] = StartStopBitPauseLength;
        int[] bits = order.getInst();

        for (int i =0; i<bits.length; i++){
            irPattern[2*i+2] = IRSignal;
            irPattern[2*i+3] = (bits[i] == 1) ?  HighBitPauseLength :  LowBitPauseLength;
        }

        return irPattern;
    }
}
