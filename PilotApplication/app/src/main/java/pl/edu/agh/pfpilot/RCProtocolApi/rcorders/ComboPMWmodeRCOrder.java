package pl.edu.agh.pfpilot.RCProtocolApi.rcorders;

import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Channel;
import pl.edu.agh.pfpilot.RCProtocolApi.rcdata.ComboPWMModeData;

/**
 * Created by Przemek on 2015-03-31.
 */
public class ComboPMWmodeRCOrder extends RCOrder {

    /**
     * Implementation of the Combo PWM Mode.
     *
     * @param channel - channel
     * @param address - 0 default address space, 1 extra address space
     * @param dataMotorA - data for motor A
     * @param dataMotorB - data for motor B
     */
    public ComboPMWmodeRCOrder(Channel channel, int address, ComboPWMModeData dataMotorA, ComboPWMModeData dataMotorB){
        this.setToggle(address);
        this.setEscape(1);
        this.setChannel(channel);

        nibble2 = dataMotorA.getData();
        nibble3 = dataMotorB.getData();

        this.setLrcCheck();
        this.fillInstance();
    }
}







