package pl.edu.agh.pfpilot.RCProtocolApi.rcorders;

import android.util.Log;

import java.util.Arrays;

import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Channel;

/**
 * Created by Przemek on 2015-03-31.
 *
 *    int toggle; // 0-1 Toggling for every new command
 *    int escape; // 0 Use “Mode” to select the modes listed below 1 Combo PWM mode
 *    int[] channel; // 00 01 10 11 Channels switch 1 - 4
 *    int address; // 0 Default address space (from power up)  1 Extra address space
 *    int[] mode; // 000 Extended mode   001 Combo direct mode   01x Reserved   1xx Single output mode
 *    int[] data; // 0000-1111 Data: different meaning depending on “Mode”
 *    int[] lrc; // xxxx = 0xF xor Nibble 1 xor Nibble 2 xor Nibble 3
 *
 */
public class RCOrder {

    /**
     * All bits in one array - full binary command
     */
    int[] inst;
    int[] nibble1;
    int[] nibble2;
    int[] nibble3;
    int[] lrc;

    //region Constructor

    public RCOrder(){
        nibble1 = new int[4];
        nibble2 = new int[]{0, 0, 0, 1};
    }
    //endregion

    public int[] getInst() {
        return inst;
    }

    protected void fillInstance() {
        try{
            inst = new int[16];
            System.arraycopy(nibble1, 0, inst, 0, 4);
            System.arraycopy(nibble2, 0, inst, 4, 4);
            System.arraycopy(nibble3, 0, inst, 8, 4);
            System.arraycopy(lrc, 0, inst, 12, 4);
        }
        catch (ArrayIndexOutOfBoundsException e){
            Log.e("UWAGA! ERROR: ", "----------------------------------------------");
            Log.e("BITS: ", Arrays.toString(inst));
            Log.e("RCORDER","Nibble: " + Arrays.toString(nibble1) + " " + Arrays.toString(nibble2) + " " + Arrays.toString(nibble3));
            Log.e("RCORDER","Suma kontrolna: " + Arrays.toString(lrc));
        }
    }

    protected void setToggle(int toggle){
        nibble1[0] = toggle;
    }

    protected void setEscape(int escape){
        nibble1[1] = escape;
    }

    protected void setChannel(Channel chanel){
        System.arraycopy(chanel.getChannel(), 0, nibble1, 2, 2);
    }

    protected void setLrcCheck() {
        lrc = lrcCheck();
    }

    private int[] lrcCheck() {
        int[] xfff = new int[] { 1, 1, 1, 1 };
        return XOR(XOR(XOR(xfff, nibble1), nibble2), nibble3);
    }

    private int[] XOR(int[] array1, int[] array2){
        int[] arrayXOR = new int[4];
        int i = 0;
        for (int b : array1){
            arrayXOR[i] = b ^ array2[i++];
        }

        return  arrayXOR;
    }
}

