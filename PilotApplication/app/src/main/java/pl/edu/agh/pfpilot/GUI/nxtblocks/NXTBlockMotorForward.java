package pl.edu.agh.pfpilot.GUI.nxtblocks;

import android.content.Context;
import android.util.AttributeSet;

import pl.edu.agh.gui.R;

/**
 * Created by MagdaK on 2015-03-28.
 */
public class NXTBlockMotorForward extends NXTBlock {

    public NXTBlockMotorForward(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
    }

    @Override
    protected void setImages() {
        addImage("motor_forward_a", R.drawable.motor_forward_a);
        addImage("motor_forward_b", R.drawable.motor_forward_b);
        addImage("motor_forward_ab", R.drawable.motor_forward_ab);
    }

    @Override
    public int[] getCommand() {
        return rcProtocol.motorForward(this.getMotorType());
    }
}
