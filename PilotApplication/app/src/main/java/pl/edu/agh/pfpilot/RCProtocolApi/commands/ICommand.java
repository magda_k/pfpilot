package pl.edu.agh.pfpilot.RCProtocolApi.commands;

import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Channel;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Toggle;

/**
 * Created by MagdaK on 2015-03-29.
 */
public interface ICommand {
    public int[] getImplementation(Toggle toggle, Channel channel);
}
