package pl.edu.agh.pfpilot.GUI.nxtblocks;

import android.content.Context;
import android.util.AttributeSet;

import pl.edu.agh.gui.R;

/**
 * Created by MagdaK on 2015-03-28.
 */
public class NXTBlockMotorBackward extends NXTBlock {

    public NXTBlockMotorBackward(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
    }

    @Override
    protected void setImages() {
        addImage("motor_backward_a", R.drawable.motor_backward_a);
        addImage("motor_backward_b", R.drawable.motor_backward_b);
        addImage("motor_backward_ab", R.drawable.motor_backward_ab);
    }

    @Override
    public int[] getCommand() {
        return rcProtocol.motorBackward(this.getMotorType());
    }
}
