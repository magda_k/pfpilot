package pl.edu.agh.pfpilot.RCProtocolApi.commands;

import android.util.Log;

import java.lang.reflect.Array;
import java.util.Arrays;

import pl.edu.agh.pfpilot.RCProtocolApi.conversion.OrdersToIRSignalsConverter;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Channel;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.MotorType;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.OutputType;
import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Toggle;
import pl.edu.agh.pfpilot.RCProtocolApi.rcdata.SingleOutputModeData;
import pl.edu.agh.pfpilot.RCProtocolApi.rcorders.SingleOutputModeRCOrder;

/**
 * Created by MagdaK on 2015-03-29.
 */
public abstract class CommandWithMotorType implements ICommand {
    protected MotorType motorType;

    public CommandWithMotorType(MotorType motorType){
        this.motorType = motorType;
    }

    public abstract int[] getImplementation(Toggle toggle, Channel channel);

    protected int[] getSingleOutputModeRCOrderByMotor(Toggle toggle, Channel channel, SingleOutputModeData data){
        if (this.motorType == MotorType.MotorA){
            int[] irCommandForMotorA = OrdersToIRSignalsConverter.convert(new SingleOutputModeRCOrder(toggle.getToggle(), channel, OutputType.OUTPUT_A, data));
            return irCommandForMotorA;
        }
        else if (this.motorType == MotorType.MotorB){
            int[] irCommandForMotorB = OrdersToIRSignalsConverter.convert(new SingleOutputModeRCOrder(toggle.getToggle(), channel, OutputType.OUTPUT_B, data));
            return irCommandForMotorB;
        }
        else if (this.motorType == MotorType.MotorAB){
            int[] irCommandForMotorA = OrdersToIRSignalsConverter.convert(new SingleOutputModeRCOrder(toggle.getToggle(), channel, OutputType.OUTPUT_A, data));
            int[] irCommandForMotorB = OrdersToIRSignalsConverter.convert(new SingleOutputModeRCOrder(toggle.getToggle(), channel, OutputType.OUTPUT_B, data));
            int[] irCommandForMotorAB = new int[irCommandForMotorA.length + irCommandForMotorB.length + 2];

            System.arraycopy(irCommandForMotorA, 0, irCommandForMotorAB, 0, irCommandForMotorA.length);
            irCommandForMotorAB[irCommandForMotorA.length] = 1000;
            irCommandForMotorAB[irCommandForMotorA.length + 1] = 1000;
            System.arraycopy(irCommandForMotorB, 0, irCommandForMotorAB, irCommandForMotorA.length + 2, irCommandForMotorB.length);

            return irCommandForMotorAB;
        }

        return new int[]{0};
    }
}