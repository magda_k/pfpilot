package pl.edu.agh.pfpilot.RCProtocolApi.commands;

import pl.edu.agh.pfpilot.RCProtocolApi.helpers.*;
import pl.edu.agh.pfpilot.RCProtocolApi.rcdata.SingleOutputModeData;

/**
 * Created by MagdaK on 2015-03-29.
 */
public class SpeedPlus extends CommandWithMotorType {

    public SpeedPlus(MotorType motorType) {
        super(motorType);
    }

    @Override
    public int[] getImplementation(Toggle toggle, Channel channel) {
        return getSingleOutputModeRCOrderByMotor(toggle, channel, SingleOutputModeData.INCREMENT_PWM);
    }
}