package pl.edu.agh.pfpilot.RCProtocolApi.helpers;

/**
 * Created by MagdaK on 2015-05-01.
 */
public enum OutputType {
    OUTPUT_A (0),
    OUTPUT_B (1);

    private final int output;

    OutputType(int output) {
        this.output = output;
    }

    public int getOutput() {
        return output;
    }
}
