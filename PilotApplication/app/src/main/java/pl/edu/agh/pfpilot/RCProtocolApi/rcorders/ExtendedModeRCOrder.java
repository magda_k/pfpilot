package pl.edu.agh.pfpilot.RCProtocolApi.rcorders;

import pl.edu.agh.pfpilot.RCProtocolApi.helpers.Channel;
import pl.edu.agh.pfpilot.RCProtocolApi.rcdata.ExtendedModeData;

/**
 * Created by Przemek on 2015-03-31.
 */
public class ExtendedModeRCOrder extends RCOrder {

    /**
     * Implementation of the Extended Mode.
     *
     * @param toggle - toggle
     * @param channel - channel
     * @param data - data
     */
    public ExtendedModeRCOrder(int toggle, Channel channel, ExtendedModeData data) {
        this.setToggle(toggle);
        this.setEscape(0);
        this.setChannel(channel);

        nibble3 = data.getData();

        this.setLrcCheck();
    }
}
