package pl.edu.agh.pfpilot.RCProtocolApi.commands;


import pl.edu.agh.pfpilot.RCProtocolApi.helpers.*;
import pl.edu.agh.pfpilot.RCProtocolApi.rcdata.SingleOutputModeData;

/**
 * Created by MagdaK on 2015-03-29.
 */
public class MotorBackward extends CommandWithMotorType {

    public MotorBackward(MotorType motorType) {
        super(motorType);
    }

    @Override
    public int[] getImplementation(Toggle toggle, Channel channel) {
        return getSingleOutputModeRCOrderByMotor(toggle, channel, SingleOutputModeData.PWM_BACKWARD_1);
    }
}