#Inteligentny programowalny pilot do Lego Power Functions.
![PowerFunctions_Logo.png](https://bitbucket.org/repo/GRXo56/images/29127905-PowerFunctions_Logo.png)


Klocki lego mogą być zmotoryzowane oraz zelektryfikowane z wykorzystaniem elementów Power Functions, w skład których wchodzą piloty na podczerwień, odbiorniki, silniki oraz światła. Specyfikacja protokół komunikacyjnego IR jest dostępna publicznie. Celem projektu jest opracowanie biblioteki dla systemu Android implementującej protokół PF z wykorzystaniem wbudowanego nadajnika podczerwieni oraz aplikacji inteligentnego pilota umożliwiającej definiowanie sekwencji poleceń. Idea aplikacji powinna być zbliżona do rozwiązania zastosowanego w zestawie 8479.
